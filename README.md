### Contenedor del servicio Mysql

#### Running on Docker

```
docker-compose up -d
```

To destroy the containers, execute:

```
docker-compose down --rmi all
```

